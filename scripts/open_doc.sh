#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")/../"

  gio open ./hungry_penguins_common_api.docdir/index.html
)
