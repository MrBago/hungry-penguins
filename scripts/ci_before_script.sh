#!/usr/bin/env sh

set -eu

apt -yqq update
apt -yqq install ocaml opam shellcheck m4

opam init -a
# shellcheck disable=SC2046
eval $(opam config env)
opam update -y
opam install -y oasis alcotest
