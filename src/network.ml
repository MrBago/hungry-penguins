(** Network module *)

(* most of this code has been written by Sylvain Conchon *)

open Unix

let log s = print_string s

type client = file_descr

type 'a requete =
    ClientMsg of client * 'a
  | ClientDisconnect of client

exception Network_Error of string

let error e = Format.eprintf "%s\n@." e

let exit_failure () = exit 1

let get_ip hostname =
  let host =
    try gethostbyname hostname
    with
    | Not_found ->
        error ("Warning: can't find address of " ^ hostname ^ ", using local address instead");
        gethostbyname "localhost"
    | Unix_error(e, _, _) ->
        error (error_message e);
        exit_failure ()
  in
  try host.h_addr_list.(0) with Invalid_argument _ -> exit_failure ()

let server_socket = socket PF_INET SOCK_STREAM 0
let server_outchan = out_channel_of_descr server_socket
let client_socket = socket PF_INET SOCK_STREAM 0

let connect_to_server host port =
  try
    let host = if host = "" then gethostname () else host in
    let sockaddr = ADDR_INET(get_ip host, port) in
    connect server_socket sockaddr
  with Unix.Unix_error (e, _, _) ->
    let host = if host = "" then "localhost" else host in
    let m = host ^ ":" ^ (string_of_int port) in
    error (error_message e);
    raise (Network_Error("Can't connect to server on " ^ m))

let disconnect_from_server () =
  Unix.shutdown server_socket Unix.SHUTDOWN_SEND

let server_start port =
  try
    let localhost = gethostname () in
    let sockaddr = ADDR_INET(get_ip localhost, port) in
    setsockopt server_socket SO_REUSEADDR true;
    bind server_socket sockaddr;
    listen server_socket 3
  with Unix.Unix_error _ ->
    let m = "localhost:" ^ (string_of_int port) in
    raise (Network_Error("Can't start server on " ^ m))

let send_msg_to_server m =
  let s = Marshal.to_string m [] in
  ignore(send server_socket s 0 (Bytes.length s) []);
  flush server_outchan

let send_msg_to_client m client =
  let s = Marshal.to_string m [] in
  let outchan_client = out_channel_of_descr client in
  ignore(send client s 0 (Bytes.length s) []);
  flush outchan_client

let hs = Marshal.header_size
let header = Bytes.create hs

let rec read_msg_and_next_header descr =
  let len = Marshal.total_size header 0 in
  let buf = Bytes.create (len+hs) in
  Bytes.blit header 0 buf 0 hs;
  let rcv = recv descr buf hs len [] in
  let obj = Marshal.from_string buf 0 in
  if rcv < len then [obj]
  else begin
    Bytes.blit buf len header 0 hs;
    obj::(read_msg_and_next_header descr)
  end

let empty_msg_queue descr =
  if (recv descr header 0 hs []) < hs then []
  else read_msg_and_next_header descr

exception No_Msg

let get_server_msg_unsafe =
  let reste = ref [] in
  fun () ->
    match !reste with
      | m::l -> reste := l; m
      | [] ->
	  let ready, _, _ = Unix.select [server_socket] [] [] (-1.0) in
	  match ready with
	    | [] -> raise No_Msg
	    | [s] ->
    		(match empty_msg_queue s with
          | [] -> raise No_Msg
    		  | m::l -> reste := l; m)
	    | _ -> assert(false)

let rec get_server_msg () =
  try get_server_msg_unsafe ()
  with Unix.Unix_error(e, _, _) -> get_server_msg ()

let requests_clients time ls =
  let process_request s =
    let l = empty_msg_queue s in
    if l = [] then [ClientDisconnect(s)]
    else List.map (fun m -> ClientMsg(s, m)) l
  in
  let ready, _, _ = select ls [] [] 0.0001 in
  List.concat (List.map process_request ready)

let read_clients_requests = requests_clients 0.0001

let wait_client_connection () =
  let ready, _, _ = select [server_socket] [] [] (-1.0) in
  assert (List.length ready=1);
  fst (accept server_socket)

let regularly_call_fun f v =
  let _ = Sys.set_signal Sys.sigalrm (Sys.Signal_handle (fun _ -> f ())) in
  ignore(Unix.setitimer Unix.ITIMER_REAL {Unix.it_interval = v ; Unix.it_value = v})

let wait_indefinitely e =
  try
    while (true) do
      try ignore(read_line())
      with x when x <> e -> ()
    done
  with
    | x when x = e -> ()
    | _ -> assert false
