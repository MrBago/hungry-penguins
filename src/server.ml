(** Server module *)

open Game_state

(** raised when a client made an invalid action *)
exception InvalidAction

(** debug only *)
let log s = print_string s

(** initialisation function *)
let init_server () =

  (* TODO: handle this in a better way *)
  let port = 10234 in
  let nbOfPlayers = 2 in

  log ("starting a server with port = " ^ (string_of_int port) ^ " ; nbj = " ^ (string_of_int nbOfPlayers) ^ "...\n");

  Network.server_start port;

  let clients = Array.init nbOfPlayers (fun n -> log ("waiting for client " ^ (string_of_int n)^ " to connect\n"); Network.wait_client_connection ()) in

  log "the game will start soon\n";

  clients

(** read a msg from a given client *)
let get_client_msg client = Network.read_clients_requests [client]

(** handle one request from a client *)
let treat_player_action nbOfPlayers gameState playerNum actions =
  List.iter (function
      | Network.ClientDisconnect(_) -> failwith "TODO: logout";
      | Network.ClientMsg(_, msg) -> begin

          try
            let penguin, (dir, blockNb) = Scanf.sscanf msg "move %d %d %d" (fun x d y -> x, (Move.dir_of_int d, y)) in

            let penguinsPerPlayer = Game_state.penguins_per_player nbOfPlayers in

            if penguin >= penguinsPerPlayer || penguin < 0 then raise InvalidAction;

            let penguinNum = playerNum * penguinsPerPlayer + penguin in

            let _, penguins = gameState in
            let (pos, _) = penguins.(penguinNum) in
            if not (Move.is_valid_move gameState playerNum pos dir blockNb ) then
              raise InvalidAction
            else
              let grid, _ = gameState in
              let newGrid, newPenguins = Move.apply_move penguinNum blockNb dir grid penguins in
              Board.replace_content grid newGrid;

              let sizeP = Array.length penguins in
              let newSizeP = Array.length newPenguins in

              if sizeP != newSizeP then failwith "invalid new penguins from apply_move";

              for i = 0 to sizeP - 1 do
                penguins.(i) <- newPenguins.(i)
              done;

              ()
          with _ -> raise InvalidAction
        end
    ) actions

(** handle one turn for one client *)
let rec treat_turn gameState clients turn nbOfPlayers =
  Network.send_msg_to_client gameState clients.(turn);
  let rec wait_msg () =
    let msg = get_client_msg clients.(turn) in
    match msg with
    | [] -> wait_msg ()
    | _ -> msg
  in
  let msg = wait_msg () in
  try treat_player_action nbOfPlayers gameState turn msg
  with InvalidAction -> treat_turn gameState clients turn nbOfPlayers

(** main function *)
let _ =

  let clients = init_server () in

  (* TODO: fix this *)
  let nbOfPlayers = (Array.length clients) in

  Array.iteri (fun num client -> Network.send_msg_to_client (nbOfPlayers, num) client) clients;

  let rec mainloop gameState turn =
    if not (Game_state.game_over gameState) then begin
      treat_turn gameState clients turn nbOfPlayers;
      mainloop gameState ((turn + 1) mod nbOfPlayers)
    end
  in

  let initialGameState = Game_state.init nbOfPlayers in

  mainloop initialGameState 0;

  ()
