(** Player module *)

(** represents a player *)
type player = Red | Blue | Green | Yellow

(** raised if we ask an invalid int -> player computation *)
exception InvalidPlayerIndex

(** used to initialize various things *)
let empty = Red

(** return an int as a player *)
let of_num = function
  | 0 -> Red
  | 1 -> Blue
  | 2 -> Green
  | 3 -> Yellow
  | _ -> raise InvalidPlayerIndex

(** return a player as a string *)
let to_string = function
  | Red -> "R"
  | Blue -> "B"
  | Green -> "G"
  | Yellow -> "Y"
