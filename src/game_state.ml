(** Game_state module *)

type penguins = Penguin.penguin array
type game_state = Board.grid * penguins
type block_content = Fish of Board.fish | Player of Player.player

exception InvalidPlayerNumber
exception PlayerFound of Player.player

(** compute the list of penguins of a player from a player and the list of all penguins *)
let playerspenguins (joueur:Player.player) (manchots: penguins)=
	let aux p= (snd(p)=joueur) in
  let l = Array.fold_left (fun acc el -> if aux el then el::acc else acc) [] manchots in
  List.rev l

(** compute the nb. of penguins per player from the number of players *)
let penguins_per_player = function
  | 2 -> 4
  | 3 -> 3
  | 4 -> 2
  | _ -> raise InvalidPlayerNumber

(** create the initial game state *)
let init nbOfPlayers =

  let penguinsPerPlayer = penguins_per_player nbOfPlayers in

  let nbOfPenguins = penguinsPerPlayer * nbOfPlayers in

  let penguins = Array.make nbOfPenguins Penguin.empty in

  let sizeX, sizeY = 4, 15 in

  let get_random_pos () =
    Random.int(sizeX), Random.int(sizeY)
  in

  let rec set_penguin i j =
    let pos = get_random_pos () in
    if Array.exists (fun (pos', _) -> pos' = pos) penguins then
      set_penguin i j
    else begin
      Array.set penguins (penguinsPerPlayer * i + j) (pos, Player.of_num i);
      pos
    end
  in

  let grid = Board.empty_grid sizeX sizeY in

  for i = 0 to sizeY - 1 do
    for j = 0 to sizeX - 1 do
      grid.(j).(i)<- Random.int(3)+1;
    done;
  done;

  for i = 0 to nbOfPlayers - 1 do
    for j = 0 to penguinsPerPlayer - 1 do
      let posX, posY = set_penguin i j in
      grid.(posX).(posY) <- 0;
    done;
  done;

  (* TODO: each player choose where to put its penguins instead of random ? *)

  grid, penguins

(* TODO: fix this *)
let game_over gameState = false

(** return the player on position pos if there's one *)
let get_player_on pos penguins =
  try Array.iter (fun (pos', player) ->
      if pos = pos' then raise (PlayerFound player)
    ) penguins; None
  with PlayerFound p -> Some p

(** return what's on a block *)
let get_block_content pos (grid, penguins) =
  match get_player_on pos penguins with
  | None -> Fish (Board.get_fish_nb pos grid)
  | Some player -> Player player

(** return a block content as a string *)
let block_content_to_string = function
  | Player p -> Player.to_string p
  | Fish f -> Board.fish_to_string f

(** get the block content on position pos and return it as a string *)
let get_block_content_as_string pos gameState =
  block_content_to_string (get_block_content pos gameState)

(** return a game state as a string *)
let to_string (grid, penguins) =

  let colNb = Board.get_col_nb grid in
  let lineNb = Board.get_line_nb grid in

  let res = ref "" in

  for i = 0 to lineNb - 1 do
    if i mod 2 = 0 then begin
      for j = 0 to colNb - 1 do
        let content = get_block_content_as_string (j, i) (grid, penguins) in
        res := !res ^ "\\_/" ^ content;
      done;
      res := !res ^ "\\\n";
    end else begin
      for j = 0 to colNb -1 do
        let content = get_block_content_as_string (j, i) (grid, penguins) in
        res := !res ^ "/" ^ content ^ "\\_";
      done;
      res := !res ^ "/\n";
    end
  done;

  if lineNb mod 2 = 0 then begin
    for j = 0 to colNb - 1 do
      res := !res ^ "\\_/ ";
    done;
    res := !res ^ "\\\n";
  end else begin
    for j = 0 to colNb -1 do
      res := !res ^ "/ \\_";
    done;
    res := !res ^ "/\n";
  end;

  !res
