(** Board module *)

(** represents a nb of fish on a block *)
type fish = int

(** represents a grid of fish *)
type grid = fish array array

(** represents a position on the grid *)
type pos = int * int

(** raised when we detect something's wrong with grid size *)
exception InvalidGridSize

(** used to initialize various things *)
let empty_pos = 0, 0

(** create a grid of size h*w with 0 fish on each block *)
let empty_grid h w = Array.make_matrix h w 0

(** return the nb of col. in a grid *)
let get_col_nb grid = Array.length grid

(** return the nb of lines in a grid *)
let get_line_nb grid =
  if Array.length grid > 0 then
    Array.length (grid.(0))
  else raise InvalidGridSize

(** return the nb of fish at pos (x, y) on a grid *)
let get_fish_nb (x, y) grid = grid.(x).(y)

(** return a string from a fish *)
let fish_to_string f = string_of_int f

(** update the grid old with the content of the grid new_ *)
let replace_content old new_ =

  let sizeX, sizeY = get_col_nb old, get_line_nb old in
  let newSizeX, newSizeY = get_col_nb new_, get_line_nb new_ in

  if sizeX != newSizeX || sizeY != newSizeY then raise InvalidGridSize;

  for x = 0 to sizeX - 1 do
    for y = 0 to sizeY - 1 do
      old.(x).(y) <- new_.(x).(y)
    done
  done
