(** Test file with tests, because tests are great *)

(** The tests **)

let compare_paire x y s1 s2=
	Alcotest.(check int) s1 (fst(x)) (fst(y));
	Alcotest.(check int) s2 (snd(x)) (snd(y))

let rec compare_paire_list x y s =
	match (x,y) with
	([], []) -> ()
	|([], t::q)-> Alcotest.(check int) "même longueur" 0 1
	|(t::q, [])-> Alcotest.(check int) "même longueur" 0 1
	|(t1::q1, t2::q2) -> (compare_paire t1 t2 s s; compare_paire_list q1 q2 s)


let compare_triplet x y s1 s2 s3=
	let (a,b,c) =x in
	let (d,e,f) = y in
        let boolean = (a=d) in
	Alcotest.(check bool) s3 true boolean;
	Alcotest.(check int) s2 b e;
	Alcotest.(check int) s1 c f


(** Basic test *)
let basic () =
  let expected = "jaimeleschips" in
  let got = "jaimeleschips" in
  Alcotest.(check string) "same string" got expected



(**Test for the Move module*)

let coord_dir_test1 () =
  let expected = (0,-1) in
  let got = Move.coord_dir NE (2,3) in
  compare_paire expected got "same first element" "same second element"

let coord_dir_test2 () =
  let expected = (0,1) in
  let got = Move.coord_dir SW (1,4) in
  compare_paire expected got "same first element" "same second element"

let coord_add_test1 () =
  let expected = (3,2) in
  let got = Move.coord_add (2,1) (1,1) in
  compare_paire expected got "same first element" "same second element"

let coord_add_test2 () =
  let expected = (5,6) in
  let got = Move.coord_add (2,4) (3,2) in
  compare_paire expected got "same first element" "same second element"

let coord_add_test3 () =
  let expected = (0,2) in
  let got = Move.coord_add (-1,0) (1,2) in
  compare_paire expected got "same first element" "same second element"


let coord_add_test4 () =
  let expected = (1,0) in
  let got = Move.coord_add (0,-2) (1,2) in
  compare_paire expected got "same first element" "same second element"

let check_player_move_test1 () =
  let expected = false in
  let got = Move.check_player_move 1 1 [|((1,2),Player.Red)|] in
  Alcotest.(check bool) "correct answer" expected got

let check_player_move_test2 () =
  let expected = true in
  let got = Move.check_player_move 1 2 [|((1,2), Player.Red)|] in
  Alcotest.(check bool) "correct answer" expected got

let check_player_move_test3 () =
  let expected = true in
  let got = Move.check_player_move 1 1 [|((1,2), Player.Red);((1,1), Player.Red)|] in
  Alcotest.(check bool) "correct answer" expected got

let check_player_move_test4 () =
  let expected = true in
  let got = Move.check_player_move 1 0 [|((1,2), Player.Red);((1,1), Player.Red)|] in
  Alcotest.(check bool) "correct answer" expected (not(got))

let check_move_test () =
  let expected = [(1,4)] in
  let g = Board.empty_grid 4 15 in
  g.(1).(4)<- 1;
  let got = Move.check_mvt g [|((1,6), Player.Red)|] (1,6) Move.N in
  compare_paire_list expected got "accessibility is correct"

let check_move_test2 () =
  let expected = [] in
  let g = Board.empty_grid 4 15 in
  g.(1).(0)<- 1;
  g.(1).(2)<- 1;
  let got = Move.check_mvt g [|((1,6), Player.Red)|] (1,6) Move.N in
  compare_paire_list expected got "accessibility is correct"
 
let check_move_test3 () =
  let expected = [(1,4);(1,2);(1,0)] in
  let g = Board.empty_grid 4 15 in
  g.(1).(0) <- 1;
  g.(1).(2)<- 1;
  g.(1).(4)<- 1;
  let got = Move.check_mvt g [|((1,6), Player.Red)|] (1,6) Move.N in
  compare_paire_list expected got "accessibility is correct"

let check_move_test4 () =
  let expected = [(1,6);(1,4);(1,2)] in
  let g = Board.empty_grid 4 15 in
  g.(1).(6) <- 1;
  g.(1).(2)<- 1;
  g.(1).(4)<- 1;
  let got = Move.check_mvt g [|((1,8), Player.Red)|] (1,8) Move.N in
  compare_paire_list expected got "accessibility is correct"

let check_move_test5 () =
  let expected = [(1,0)] in
  let g = Board.empty_grid 4 15 in
  g.(1).(0)<- 1;
  let got = Move.check_mvt g [|((1,2), Player.Red)|] (1,2) Move.N in
  compare_paire_list expected got "accessibility is correct"

let apply_move_test () =
  let g = Board.empty_grid 4 15 in
  g.(1).(2)<- 1;
  g.(1).(4)<- 1;
  let p = [|((1,6), Player.Red)|] in
  let (a,b) = Move.apply_move 0 2 (Move.N) g p in
  let expected = (1,2) in
  let got = fst(b.(0)) in
  compare_paire expected got "same first element" "same second element"

let apply_move_test2 () =
  let g = Board.empty_grid 4 15 in
  g.(2).(4)<- 1;
  g.(2).(5)<- 1;
  let p = [|((1,6), Player.Red)|] in
  let (a,b) = Move.apply_move 0 2 (Move.NE) g p in
  let expected = (2,4) in
  let got = fst(b.(0)) in
  compare_paire expected got "same first element" "same second element"


(**Set for the Computer module*)


let max_fish_test ()=
  let g = Board.empty_grid 4 15 in
  g.(2).(4)<- 1;
  g.(2).(5)<- 2;
  g.(3).(9) <- 1;
  let got = Computer.max_fish [(2,4);(2,5);(3,9)] (g, [||]) 0 0 0 in
  let expected = (1,2) in
  compare_paire expected got "same index" "same max"

let choose_distance_test() =
  let g = Board.empty_grid 4 15 in
  g.(1).(0)<- 1;
  g.(1).(6)<- 1;
  g.(1).(2)<- 1;
  g.(1).(4)<- 2;
  let pingu =((1,8), Player.Red) in
  let p = [|pingu|] in
  try
    let got = Computer.choose_distance (g,p) pingu Move.N Computer.max_fish in
    let expected  = (2,2) in
    compare_paire expected got "same distance" "same max"
  with
  | Computer.Pasparla -> Alcotest.(check int) "chemin possible" 0 1

let choose_direction_test () =
  let g = Board.empty_grid 4 15 in
  g.(1).(6)<- 1;
  g.(1).(2)<- 1;
  g.(1).(4)<- 2;
  g.(2).(7) <- 1;
  g.(2).(6) <- 3;
  let pingu =((1,8), Player.Red) in
  let p = [|pingu|] in
  let expected = (Move.NE, 2, 3) in
  try
    let got = Computer.choose_direction (g,p) pingu Computer.max_fish in
    compare_triplet expected got "même direction" "même distance" "même valeur"
  with
  |Computer.Pascuila -> Alcotest.(check int) "le pingouin peut bouger" 0 1

(**Set for the end game test*)


let test_end_test () =
	let g = Board.empty_grid 4 15 in
	g.(1).(1) <- 1;
	g.(1).(2) <- 2 ;
	let m = [|((1,3), Player.Red)|] in
	let b = (End_test.end_test (g,m)).(0) in
	Alcotest.(check bool) "same results" b true

let test_end_test2 () =
	let g = Board.empty_grid 4 15 in
	g.(1).(3) <- 1;
	g.(1).(2) <- 2 ;
	let m = [|((1,1), Player.Red); ((1,5), Player.Red)|] in
	let b = (End_test.end_test (g,m)).(0) in
	Alcotest.(check bool) "same results" b false

let test_end_test3 () =
	let g = Board.empty_grid 4 15 in
	g.(1).(3) <- 1;
	g.(1).(7) <- 2 ;
	let m = [|((1,1), Player.Red); ((1,9), Player.Red)|] in
	let b = (End_test.end_test (g,m)).(0) in
	let b2 = (End_test.end_test (g,m)).(1) in
	Alcotest.(check bool) "same results" b true;
	Alcotest.(check bool) "same results" b2 true





let end_game_test () =
	let g = Board.empty_grid 4 15 in
	g.(1).(3) <- 1;
	g.(1).(2) <- 1;
	g.(1).(5) <- 1;
	g.(1).(7) <- 1;
	g.(1).(9) <- 1;
	g.(1).(6) <- 1;
	g.(2).(5) <- 1;
	let m = [|((1,11), Player.Red)|] in
	let b = (End_test.end_test (g,m)).(0) in
	Alcotest.(check bool) "same results" true b;
	let n = End_game.end_game (g,m) (1,11) in
	Alcotest.(check int) "same results" 7 n



(** Sets of test *)

(** Set of basic tests *)
let set_basic = [
  "Basic", `Quick, basic;
]

let set_move = [
  "coord_dir1", `Quick, coord_dir_test1;
  "coord_dir2", `Quick, coord_dir_test2;
  "coord_add1", `Quick, coord_add_test1;
  "coord_add2", `Quick, coord_add_test2;
  "coord_add3", `Quick, coord_add_test3;
  "coord_add4", `Quick, coord_add_test4;
  "check_player_move1", `Quick, check_player_move_test1;
  "check_player_move2", `Quick, check_player_move_test2;
  "check_player_move3", `Quick, check_player_move_test3;
  "check_player_move4", `Quick, check_player_move_test4;
  "check_move_test", `Quick, check_move_test;
  "check_move_test2", `Quick, check_move_test2;
  "check_move_test3", `Quick, check_move_test3;
  "check_move_test4", `Quick, check_move_test4;
  "check_move_test5", `Quick, check_move_test5;
  "apply_move_test", `Quick, apply_move_test;
  "apply_move_test2", `Quick, apply_move_test2;
]

let set_computer = [
  "max_fish_test", `Quick, max_fish_test;
  "choose_distance_test", `Quick, choose_distance_test;
  "choose_direction_test", `Quick, choose_direction_test;
]

let set_end_game = [
  "end_test_1", `Quick, test_end_test;
  "end_test_2", `Quick, test_end_test2;
  "end_test_3", `Quick, test_end_test3;
  "end_game_test", `Quick, end_game_test;
]

(** Main function, run all sets of tests *)
let () =
  Alcotest.run "Tests" [
    "Basic", set_basic;
    "Move", set_move;
    "Computer", set_computer;
    "End_game", set_end_game;
  ]



